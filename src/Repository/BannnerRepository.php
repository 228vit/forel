<?php

namespace App\Repository;

use App\Entity\Bannner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Bannner|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bannner|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bannner[]    findAll()
 * @method Bannner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BannnerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bannner::class);
    }

    // /**
    //  * @return Bannner[] Returns an array of Bannner objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bannner
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
