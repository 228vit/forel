<?php

namespace App\Repository;

use App\Entity\RestrictedIP;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RestrictedIP|null find($id, $lockMode = null, $lockVersion = null)
 * @method RestrictedIP|null findOneBy(array $criteria, array $orderBy = null)
 * @method RestrictedIP[]    findAll()
 * @method RestrictedIP[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestrictedIPRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RestrictedIP::class);
    }

    public function ipStartsWith(?string $value): ?RestrictedIP
    {
        return empty($value) ? null : $this->createQueryBuilder('r')
            ->andWhere('r.ip_address LIKE :val')
            ->setParameter('val', $value.'%')
            ->setMaxResults(10)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?RestrictedIP
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
