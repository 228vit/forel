<?php

namespace App\Repository;

use App\Entity\ProductSetItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProductSetItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductSetItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductSetItem[]    findAll()
 * @method ProductSetItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductSetItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductSetItem::class);
    }

    // /**
    //  * @return ProductSetItem[] Returns an array of ProductSetItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductSetItem
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
