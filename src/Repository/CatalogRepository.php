<?php
namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatalogRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function findAllJoinProducts(): array
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.Products p')
            ->where('p.isActive = :isActive')
            ->setParameter('isActive', true)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->execute()
            ;
    }
}
