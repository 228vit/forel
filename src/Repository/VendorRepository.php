<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Vendor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Vendor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vendor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vendor[]    findAll()
 * @method Vendor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VendorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Vendor::class);
    }
    public function getCategoryVendors(Category $category)
    {
        return $this->createQueryBuilder('v')
            ->select('v')
            ->innerJoin('v.products', 'p')
            ->innerJoin('p.category', 'c')
            ->where('p.isActive = :isActive')
            ->andWhere('p.category = :category_id')
            ->setParameter('category_id', $category->getId())
            ->setParameter('isActive', true)
            ->groupBy('v.id')
            ->getQuery()
            ->getResult()
        ;
    }



//    /**
//     * @return Vendor[] Returns an array of Vendor objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vendor
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
