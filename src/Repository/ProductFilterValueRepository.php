<?php

namespace App\Repository;

use App\Entity\CategoryFilterValue;
use App\Entity\Product;
use App\Entity\ProductFilterValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProductFilterValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductFilterValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductFilterValue[]    findAll()
 * @method ProductFilterValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductFilterValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductFilterValue::class);
    }

    // /**
    //  * @return ProductFilterValue[] Returns an array of ProductFilterValue objects
    //  */
    public function findByProductAndFilter(Product $product, CategoryFilterValue $category_filter_value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.product = :product')
            ->andWhere('p.categoryFilterValue = :category_filter_value')
            ->setParameter('product', $product)
            ->setParameter('category_filter_value', $category_filter_value)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?ProductFilterValue
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
