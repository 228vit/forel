<?php


namespace App\Service;


use App\Entity\Product;
use App\Utils\BarCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

final class BarCodeGenerator
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function generate(Product $product): string
    {
        $countyCode = BarCode::RU_CODE;
        $vendorCode = str_pad($product->getVendor()->getId(),4, '0', STR_PAD_LEFT);
        $productCode = str_pad($product->getId(),5, '0', STR_PAD_LEFT);

        $newBarCode = BarCode::create($countyCode.$vendorCode.$productCode);

        $product->setBarCode($newBarCode);
        $this->em->persist($product);
        $this->em->flush();

        return new Response('done');
    }

    public function savePdf(Product $product)
    {
        if (null === $product->getBarCode()) {
            $this->generate($product);
        }


        $pdf = new \TCPDF('l', PDF_UNIT, array(58, 40), true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mudi Pekker');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetAutoPageBreak(TRUE, 0);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('freeserif', '', 13);
        $pdf->SetMargins(1, 2, 1);
        $pdf->AddPage();

        $pdf->SetFillColor(255, 255, 255);
        $txt = $product->getFullNameWithVendor();
        $pdf->MultiCell(56, 19, $txt, 1, 'L', 1, 0, '1' ,'2', true);

        $pdf->SetFont('freeserif', '', 11);
        $txt = 'Цена - ' . $product->getPrice() . 'р.';
        $pdf->MultiCell(56, 6, $txt, 1, 'C', 1, 0, '1' ,'20', true);

        $pdf->SetFillColor(127, 255, 127);
        $pdf->SetFont('helvetica', '', 16);

        // define barcode style
        $style = array(
            'position' => 'C',
            'align' => 'C',
            'stretch' => true,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $pdf->write1DBarcode($product->getBarCode(), 'EAN13', '1', '26', '56', '14', 2.2, $style, 'N');

        $pdf->Output($product->getFullNameWithVendor(). '.pdf', 'I');
        die(); // sorry
    }
}