<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PromoteUserCommand extends Command
{
    /** @var EntityManager $em */
    private $em = false;
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $this->container->get('doctrine.orm.default_entity_manager');

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('user:promote')
            ->setDescription('Promote user to superadmin role')
            ->addArgument('username', InputArgument::OPTIONAL, 'Please specify username')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $username = $input->getArgument('username');

        /** @var UserRepository $repo */
        $repo = $this->em->getRepository(User::class);

        /** @var User $user */
        $user = $repo->findOneBy(['username' => $username]);

        if (!$user) {
            throw new \Exception('Wrong username');
        }

        $user->setIsSuperadmin(true);

        try {
            $this->em->persist($user);
            $this->em->flush();
        } catch (\Exception $e) {
            $output->writeln(sprintf('Exception code: %s, Message: $s, trace: %s',
                $e->getCode(), $e->getMessage(), $e->getTraceAsString()));
        }


        $io->success('User ' . $user->getUsername() . ' been promoted as superadmin!');
    }
}
