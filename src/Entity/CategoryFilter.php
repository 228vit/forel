<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryFilterRepository")
 */
class CategoryFilter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Category|null the group this user belongs (if any)
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="filters")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * One CategoryFilter has Many CategoryFilterValues.
     * @ORM\OneToMany(targetEntity="CategoryFilterValue", mappedBy="categoryFilter", indexBy="id", cascade={"persist"})
     */
    private $filterValues;

    public function __construct()
    {
        $this->filterValues = new ArrayCollection();
    }

    public function getFilterValues()
    {
        return $this->filterValues;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }
    public function addFilterValue(CategoryFilterValue $filterValue)
    {
        if (!$this->filterValues->contains($filterValue)) {
            $this->filterValues->add($filterValue);
        }
    }

    public function removeFilterValue(CategoryFilterValue $filterValue)
    {
        $this->filterValues->removeElement($filterValue);
    }

    public function __toString()
    {
        return $this->getName();
    }

}
