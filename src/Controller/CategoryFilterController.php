<?php

namespace App\Controller;

use App\Entity\CategoryFilter;
use App\Form\CategoryFilterType;
use App\Repository\CategoryFilterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/category/filter")
 */
class CategoryFilterController extends AbstractController
{
    /**
     * @Route("/", name="category_filter_index", methods={"GET"})
     */
    public function index(CategoryFilterRepository $categoryFilterRepository): Response
    {
        return $this->render('category_filter/index.html.twig', [
            'category_filters' => $categoryFilterRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="category_filter_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $categoryFilter = new CategoryFilter();
        $form = $this->createForm(CategoryFilterType::class, $categoryFilter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categoryFilter);
            $entityManager->flush();

            return $this->redirectToRoute('category_filter_index');
        }

        return $this->render('category_filter/new.html.twig', [
            'category_filter' => $categoryFilter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_filter_show", methods={"GET"})
     */
    public function show(CategoryFilter $categoryFilter): Response
    {
        return $this->render('category_filter/show.html.twig', [
            'category_filter' => $categoryFilter,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="category_filter_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, CategoryFilter $categoryFilter): Response
    {
        $form = $this->createForm(CategoryFilterType::class, $categoryFilter);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_filter_index');
        }

        return $this->render('category_filter/edit.html.twig', [
            'category_filter' => $categoryFilter,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_filter_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CategoryFilter $categoryFilter): Response
    {
        if ($this->isCsrfTokenValid('delete'.$categoryFilter->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categoryFilter);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_filter_index');
    }
}
