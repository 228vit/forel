<?php

namespace App\Controller\Admin;

use App\Entity\Page;
use App\Filter\PageFilter;
use App\Repository\PageRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Page controller.
 */
class AdminPageController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'page';
    CONST ENTITY_NAME = 'Page';
    CONST NS_ENTITY_NAME = 'App:Page';

    /**
     * Lists all page entities.
     *
     * @Route("backend/page/index", name="backend_page_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $filter_form = $this->createForm(PageFilter::class, null, array(
            'action' => $this->generateUrl('backend_apply_filter', ['model' => self::MODEL]),
            'method' => 'POST',
        ));

        $pagination = $this->getPagination($request, $session, PageFilter::class);

        return $this->render('admin/page/index_new.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Creates a new page entity.
     *
     * @Route("backend/page/new", name="backend_page_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, ValidatorInterface $validator)
    {
        $page = new Page();
        $form = $this->createForm('App\Form\PageType', $page);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($page);
            $em->flush($page);
            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_page_edit', array('id' => $page->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/page/new.html.twig', array(
            'page' => $page,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Finds and displays a page entity.
     *
     * @Route("backend/page/{id}", name="backend_page_show")
     * @Method("GET")
     */
    public function showAction(Page $page)
    {
        $deleteForm = $this->createDeleteForm($page);

        return $this->render('admin/page/show.html.twig', array(
            'page' => $page,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing page entity.
     *
     * @Route("backend/page/{id}/edit", name="backend_page_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Page $page)
    {
        $deleteForm = $this->createDeleteForm($page);
        $editForm = $this->createForm('App\Form\PageType', $page);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('backend_page_edit', array('id' => $page->getId()));
        }
        if ($editForm->isSubmitted() && !$editForm->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/page/edit.html.twig', array(
            'page' => $page,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Deletes a page entity.
     *
     * @Route("backend/page/{id}", name="backend_page_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Page $page)
    {
        $filter_form = $this->createDeleteForm($page);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($page);
            $em->flush($page);

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_page_index');
    }

    /**
     * Creates a form to delete a page entity.
     *
     * @param Page $page The page entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Page $page)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_page_delete', array('id' => $page->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }



    /**
     * Lists all page entities.
     *
     * @Route("backend/page/index_orig", name="backend_page_index_orig")
     * @Method("GET")
     */
    public function indexOrigAction(Request $request, SessionInterface $session)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var PageRepository $repository */
        $repository = $em->getRepository('App:Page');

        $sort_by = $request->query->get('sort_by', 'id');
        $order = $request->query->get('order', 'asc');

        // todo: replace to POST
        // todo: save filter to session
        // we store different filters by different platforms
        $session_filters = $session->get('filters', array());

        $filter_form = $this->createForm(PageFilter::class, null, array(
            'action' => $this->generateUrl('backend_apply_filter', ['model' => 'page']),
            'method' => 'POST',
        ));

        if (count($session_filters) && isset($session_filters[$this->filter_session_name])) {
            $current_filters = $session_filters[$this->filter_session_name];
            $filter_form->submit($current_filters);

            $filterBuilder = $repository->createQueryBuilder('page');
            $this->get('lexik_form_filter.query_builder_updater')
                ->addFilterConditions($filter_form, $filterBuilder)
                ->orderBy('page.'.$sort_by, $order)
            ;

            $query = $filterBuilder->getQuery();
        } else {
            $current_filters = null;
            // default query w/sorting
            $query = $repository->createQueryBuilder('page')
                ->orderBy('page.'.$sort_by, $order)
                ->getQuery();
        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            self::ROWS_PER_PAGE  /*limit per page*/
        );

        $pages = $query->getResult();

        return $this->render('admin/page/index_new.html.twig', array(
            'pages' => $pages,
            'pagination' => $pagination,
            'current_filters' => $current_filters,
            'filter_form' => $filter_form->createView(),
        ));
    }

}
