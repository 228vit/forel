<?php

namespace App\Controller\Admin;

use App\Service\BarCodeGenerator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

trait AdminTraitController
{

    private $current_filters = null;
    private $filter_form;
    private $em;
    private $paginator;
    private $query_builder_updater;
    private $barCodeGenerator;

    public function __construct(EntityManagerInterface $em,
                                PaginatorInterface $paginator,
                                BarCodeGenerator $barCodeGenerator,
                                FilterBuilderUpdaterInterface $query_builder_updater)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->barCodeGenerator = $barCodeGenerator;
        $this->query_builder_updater = $query_builder_updater;
    }

    private function getPagination(Request $request,
                                   SessionInterface $session,
                                   string $filter_form_class)
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository(self::NS_ENTITY_NAME);

        $this->filter_form = $this->createForm($filter_form_class, null, array(
            'action' => $this->generateUrl('backend_apply_filter', ['model' => self::MODEL]),
            'method' => 'POST',
        ));

        /** @var Query $query */
        $query = $this->buildQuery($repository, $request, $session, $this->filter_form, self::MODEL);

        $pagination = $this->paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            self::ROWS_PER_PAGE  /*limit per page*/
        );

        return $pagination;
    }

    private function buildQuery(EntityRepository $repository, Request $request,
        SessionInterface $session, FormInterface $filter_form, string $model)
    {
        $sort_by = $request->query->get('sort_by', 'id');
        $order = $request->query->get('order', 'desc');
        $session_filters = $session->get('filters', false);

        if (false !== $session_filters && count($session_filters) && isset($session_filters[$model])) {
            $this->current_filters = $session_filters[$model];
            $filter_form->submit($this->current_filters);

            $filterBuilder = $repository->createQueryBuilder($model);
            
            $this->query_builder_updater
                ->addFilterConditions($filter_form, $filterBuilder)
                ->orderBy($model.'.'.$sort_by, $order)
            ;

            $query = $filterBuilder->getQuery();
        } else {
            $this->current_filters = null;
            // default query w/sorting
            $query = $repository->createQueryBuilder($model)
                ->orderBy($model.'.'.$sort_by, $order)
                ->getQuery();
        }

        return $query;
    }

    /**
     * Save filter values in session.
     *
     * @Route("backend/apply/filter/{model}", name="backend_apply_filter", methods={"POST"})
     */
    public function saveModelFilter(Request $request, SessionInterface $session, $model)
    {
        $filter = $request->request->get('item_filter');
        // todo: validate?
        // unset empty values
        if (is_array($filter)) {
            $filter = array_filter($filter, function($value) { return $value !== ''; });
            // csrf???
            unset($filter['_token']);
        }

        // save filter to session
        $session->set('filters', array(
            $model => $filter,
        ));

        $referer = $request->headers->get('referer');
        $urlParts = parse_url($referer);
        unset($urlParts['query']);

        $url = $urlParts['scheme'].'//'.$urlParts['host'].$urlParts['path'];
        // redirect to referer
        return $this->redirectToRoute("backend_{$model}_index");
    }

    /**
     * @Route("backend/reset/filter/{model}", name="backend_reset_filter")
     */
    public function resetModelFilter(Request $request, SessionInterface $session, $model)
    {
        $current_filters = $session->get('filters', false);
        if (isset($current_filters[$model])) {
            unset($current_filters[$model]);
        }

        $session->set('filters', $current_filters);
        $referer = $request->headers->get('referer');
        $urlParts = parse_url($referer);
        unset($urlParts['query']);

        $url = $urlParts['scheme'].'//'.$urlParts['host'].$urlParts['path'];

        // redirect to referer
//        return $this->redirect($request->headers->get('referer'));
        return $this->redirectToRoute("backend_{$model}_index");
    }


}