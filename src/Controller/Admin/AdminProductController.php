<?php

namespace App\Controller\Admin;

use App\Entity\CategoryProductParameter;
use App\Entity\Product;
use App\Entity\ProductFilterValue;
use App\Entity\ProductImage;
use App\Entity\ProductParameter;
use App\Entity\Vendor;
use App\Filter\ProductFilter;
use App\Form\ProductType;
use App\Repository\CategoryFilterValueRepository;
use App\Repository\ProductFilterValueRepository;
use App\Repository\ProductRepository;
use App\Repository\VendorRepository;
use App\Service\BarCodeGenerator;
use App\Utils\FormErrors;
use App\Utils\Slugger;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Picqer\Barcode\BarcodeGeneratorHTML;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Util\FormUtil;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\FileUploader;
use App\Utils\BarCode;

class AdminProductController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 30;
    CONST MODEL = 'product';
    CONST ENTITY_NAME = 'Product';
    CONST NS_ENTITY_NAME = 'App:Product';


    /**
     * @Route("backend/product/toggle/field", name="ajax_product_toggle_field", methods={"GET"})
     */
    public function ajaxToggleFieldAction(Request $request)
    {
        $id = $request->query->get('id', false);
        $field = $request->query->get('field', false);

        $em = $this->getDoctrine()->getManager();

        /** @var Product $product */
        $product = $em->getRepository(Product::class)->find($id);

        if (false === $product) {
            return new JsonResponse('Wrong ID', 400);
        }

        switch ($field) {
            case 'showOnMarket':
                $status = !$product->isShowOnMarket();
                $product->setShowOnMarket($status);
                break;
            case 'isShowOnTop':
                $status = !$product->isShowOnTop();
                $product->setShowOnTop($status);
                break;
            case 'isActive':
                $status = !$product->isActive();
                $product->setIsActive($status);
                break;
            case 'inAction':
                $status = !$product->isInAction();
                $product->setInAction($status);
                break;
            default:
                return new Response('<span class="badge badge-pill badge-secondary"><i class="fa fa-question"></i></span>', 200);
                break;
        }

        $em->persist($product);
        $em->flush();

        $response = sprintf('<span class="badge badge-pill badge-%s"><i class="fa fa-%s"></i></span>',
            ($status ? 'success': 'danger'),
            ($status ? 'check-square': 'window-close')
        );

        return new Response($response, 200);
    }

    /**
     * @Route("backend/product/generate_slug", name="ajax_product_generate_slug", methods={"GET"})
     */
    public function ajaxGenerateSlugAction(Request $request,
                                           ProductRepository $productRepository,
                                           VendorRepository $vendorRepository)
    {
        $this->addFlash('success', 'Slugify banned!');
        return $this->redirectToRoute('backend_product_index');
    }

    /**
     * @Route("backend/product/filter_value", name="ajax_product_filter_value", methods={"GET"})
     */
    public function ajaxFilterValueAction(Request $request, ProductFilterValueRepository $repository,
                                          ProductRepository $productRepository,
                                          CategoryFilterValueRepository $categoryFilterValueRepository,
                                          EntityManagerInterface $em)
    {
        $product_id = $request->query->get('product_id', false);
        $filter_id = $request->query->get('filter_id', false);
        $value = (bool)$request->query->get('value', false);
        $product = $productRepository->find($product_id);
        $categoryFilterValue = $categoryFilterValueRepository->find($filter_id);
        $filter = $categoryFilterValueRepository->find($filter_id);

        if (null === $filter OR null === $product) {
            return new JsonResponse('Product or Filter not found', 404);
        }

        $productFilterValue = $repository->findByProductAndFilter($product, $filter);

        if (null !== $productFilterValue && false === $value) {
            $em->remove($productFilterValue);
        }

        if (null === $productFilterValue && true === $value) {
            $productFilterValue = (new ProductFilterValue())
                ->setProduct($product)
                ->setCategoryFilterValue($categoryFilterValue)
            ;
            $em->persist($productFilterValue);
        }

        $em->flush();

        return new JsonResponse('ok', 200);
    }

    /**
     * @Route("backend/product/ajax/price", name="ajax_product_get_price", methods={"GET"})
     */
    public function ajaxGetProductPriceAction(Request $request)
    {
        $id = $request->query->get('id', false);
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);

        if (false === $product) {
            return new JsonResponse('Wrong ID', 400);
        }

        return new JsonResponse([
                'price' => $product->getPrice(),
                'priceIn' => $product->getPriceIn(),
            ], 200);
    }

    /**
     * @Route("backend/product/change/status", name="ajax_product_status", methods={"GET"})
     */
    public function ajaxStatusAction(Request $request)
    {
        $id = $request->query->get('id', false);

        $em = $this->getDoctrine()->getManager();

        $product = $em->getRepository(Product::class)->find($id);

        if (false === $product) {
            return new JsonResponse('Wrong ID', 400);
        }

        $status = $request->query->get('status');
        $product->setStatus($status);

        $em->persist($product);
        $em->flush($product);

        return new JsonResponse('ok', 200);
    }

    /**
     * Lists all product entities.
     *
     * @Route("backend/product/index", name="backend_product_index", methods={"GET"})
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, ProductFilter::class);

        return $this->render('admin/product/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'statuses' => Product::getStatuses(),
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'product.id',
                    'sortable' => true,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'product.name',
                    'sortable' => true,
                ],
                'a.price' => [
                    'title' => 'Price',
                    'row_field' => 'price',
                    'sorting_field' => 'product.price',
                    'sortable' => true,
                ],
                'a.pic' => [
                    'title' => 'Pic',
                    'row_field' => 'pic',
                    'sorting_field' => 'product.pic',
                    'sortable' => false,
                ],
                'a.isActive' => [
                    'title' => 'Is active?',
                    'row_field' => 'isActive',
                    'sorting_field' => 'product.isActive',
                    'sortable' => false,
                ],
            ]
        ));
    }

    /**
     * @Route("backend/product/bar_codes/empty", name="backend_product_bar_codes_empty", methods={"GET"})
     */
    public function emptyBarCodes(ProductRepository $productRepository)
    {
        $rows = $productRepository->findBy([
            'barCode' => '-',
        ]);

        return $this->render('admin/product/emptyBarCodes.html.twig', array(
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'statuses' => Product::getStatuses(),
            'rows' => $rows,
        ));
    }

    /**
     * @Route("backend/product/bar_codes/generate/{slug}", name="backend_product_bar_codes_generate", methods={"GET"})
     */
    public function generateByVendor(Vendor $vendor,
                                     ProductRepository $productRepository,
                                     BarCodeGenerator $barCodeGenerator)
    {
        $rows = $productRepository->findBy([
            'barCode' => '-',
        ]);

        /** @var Product $row */
        foreach ($rows as $row) {

        }

        return $this->render('admin/product/emptyBarCodes.html.twig', array(
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'statuses' => Product::getStatuses(),
            'rows' => $rows,
        ));
    }

    /**
     * @Route("backend/product/bar_codes", name="backend_product_bar_codes", methods={"GET"})
     */
    public function barCodesListAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, ProductFilter::class);

        return $this->render('admin/product/barCodes.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'statuses' => Product::getStatuses(),
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'product.id',
                    'sortable' => true,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'product.name',
                    'sortable' => true,
                ],
                'a.price' => [
                    'title' => 'Price',
                    'row_field' => 'price',
                    'sorting_field' => 'product.barCode',
                    'sortable' => true,
                ],
                'a.pic' => [
                    'title' => 'Pic',
                    'row_field' => 'pic',
                    'sorting_field' => 'product.pic',
                    'sortable' => false,
                ],
                'a.isActive' => [
                    'title' => 'Is active?',
                    'row_field' => 'isActive',
                    'sorting_field' => 'product.isActive',
                    'sortable' => false,
                ],
            ]
        ));
    }


    /**
     * Creates a new product entity.
     *
     * @Route("backend/product/new", name="backend_product_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, ProductRepository $productRepository,
                              ValidatorInterface $validator, FileUploader $fileUploader)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $product->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $product->setPic($fileName);
            }

            $product->setSlug($this->makeSlug($product, $productRepository));

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_product_edit', array('id' => $product->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    private function makeSlug(Product $product, ProductRepository $productRepository)
    {
        $slug = Slugger::urlSlug(sprintf('%s %s',
            $product->getVendor()->getSlug(), $product->getName()), array('transliterate' => true)
        );

        while($productRepository->slugExists($slug)) {
            $slug .= '-' . rand(1000, 9999);
        }

        return $slug;
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/product/{id}/clone", name="backend_product_clone", methods={"GET"})
     */
    public function cloneAction(Request $request, Product $product, ProductRepository $productRepository, BarCodeGenerator $barCodeGenerator)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Product $new_product */
        $new_product = clone $product;
        $new_product->setSlug($this->makeSlug($new_product, $productRepository));
        $em->persist($new_product);
        $em->flush();

        $barCodeGenerator->generate($new_product);

        /** @var ProductFilterValue $productFilterValue */
        foreach ($product->getProductFilterValues() as $productFilterValue) {
            $newProductFilterValue = clone $productFilterValue;
            $newProductFilterValue->setProduct($new_product);
            $em->persist($newProductFilterValue);
        }

        $em->flush();

        $this->addFlash('success', 'Product was cloned successfully!');
        return $this->redirectToRoute('backend_product_edit', array('id' => $new_product->getId()));
    }


    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/products/update", name="backend_products_update", methods={"POST"})
     */
    public function massUpdate(Request $request)
    {
        $items = $request->request->get('product');

        $em = $this->getDoctrine()->getManager();

        foreach ($items as $id => $item) {
            /** @var Product $product */
            $product = $em->getRepository(Product::class)->find($id);
            if (false === $product) {
                continue;
            }
            $product->setPrice($item['price'])
                ->setPriceOld($item['price_old'])
                ->setPriceOpt($item['price_opt'])
                ->setPriceIn($item['price_in'])
            ;

            $em->persist($product);
            $em->flush();
        }

        $this->addFlash('success', 'Your changes were saved!');

        // redirect to referer
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Всем товарам сгенерировать slug
     *
     * @Route("backend/products/slugify", name="backend_products_slugify", methods={"GET"})
     */
    public function slugifyAction(Request $request, ProductRepository $productRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $products = $productRepository->getWithVendor();

        foreach ($products as $product) {
            /** @var Product $product */
            if (!empty($product->getSlug())) {
                continue;
            }

            $product->setSlug($this->makeSlug($product, $productRepository));

            $em->persist($product);
        }

        $em->flush();
        die();

        $this->addFlash('success', 'Your changes were saved!');

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/product/wtf", name="backend_product_wtf", methods={"GET", "POST"})
     */
    public function wtfAction(Request $request,
                               ProductRepository $productRepository,
                               FileUploader $fileUploader,
                               EntityManagerInterface $em
    )
    {

        return $this->render('admin/product/wtf.html.twig');

    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/product/{id}/edit", name="backend_product_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request,
                         Product $product,
                         ProductRepository $productRepository,
                         FileUploader $fileUploader,
                         EntityManagerInterface $em
    )
    {
        $originalImages = new ArrayCollection();

        foreach ($product->getImages() as $image) {
            if (false === $product->getImages()->contains($image)) {
                $originalImages->add($image);
            }
        }

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $product->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $product->setPic($fileName);
            }

            if (empty($product->getSlug())) {
                $product->setSlug($this->makeSlug($product, $productRepository));
            }

            foreach ($originalImages as $image) {
                if (false === $product->getImages()->contains($image)) {
                    // remove the Task from the Tag
                    $product->getImages()->removeElement($image);
                    // todo: check if image deleted
                    $em->persist($product);
                    $em->remove($image);
                }
            }

            // save and create new images
            $images = $form['images']->getData();

            /** @var ProductImage $image */
            foreach ($images as $i => $image) {
                $picFile = $form['images'][$i]['pic']->getData();
                if ($picFile) {
                    $fileName = $fileUploader->upload($picFile);
                    $image->setPicFileName($fileName);
                    $image->setProduct($product);
                    $originalImages->add($image);
                }
            }

            $em->persist($product);
            $em->flush();

            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('backend_product_edit', array('id' => $product->getId()));
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due saving object! ' . FormErrors::toString($form));
        }

        /** @var CategoryProductParameter $parameter */
//        foreach ($product->getCategory()->getCategoryProductParameters() as $parameter) {
//            $found = false;
//            foreach ($product->getProductParameters() as $productParameter) {
//                if ($productParameter->getName() === $parameter->getName()) {
//                    $found =  true;
//                    break;
//                }
//            }
//
//            if (! $found) {
//                $productParameter = (new ProductParameter())
//                    ->setName($parameter->getName())
//                    ->setValue('-')
//                ;
//                // внутри метод сам проверит на дубликат
//                $product->addProductParameter($productParameter);
//            }
//        }

        // todo: create form again?
        $form = $this->createForm(ProductType::class, $product);

        $productFilters = [];
        /** @var ProductFilterValue $item */
        foreach ($product->getProductFilterValues()->toArray() as $item) {
            $productFilters[] = $item->getCategoryFilterValue()->getId();
        }

        // фильтры категории для товара
        $categoryFilters = $product->getCategory()->getFilters();

        $deleteForm = $this->createDeleteForm($product);

        return $this->render('admin/product/edit.html.twig', array(
            'row' => $product,
            'productFilters' => $productFilters,
            'categoryFilters' => $categoryFilters,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * @Route("backend/product/barcode/generate/{id}", name="backend_product_barcode_generate", methods={"GET"})
     */
    public function generateBarCode(Product $product, Request $request, BarCodeGenerator $barCodeGenerator)
    {
        $barCodeGenerator->generate($product);

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("backend/product/barcode/{id}", name="backend_product_barcode_show", methods={"GET"})
     */
    public function showBarcodeAction(Product $product)
    {

        $this->barCodeGenerator->savePdf($product);
    }

    public function showBarCodeHtmlAction(Product $product)
    {
        $generator = new BarcodeGeneratorHTML();

        $barCode = '<div style=" padding: 2px 10px 2px 10px; border: 1px solid; width: 250px; text-align: center; font-size: 9px; font-family: monospace;">' . PHP_EOL;
        $barCode .= $generator->getBarcode($product->getBarCode(), $generator::TYPE_CODE_128, 2, 20);
        $barCode .= '<div style="text-align: center;">'.$product->getBarCode().'</div>';
        $barCode .= '<div style="text-align: center;">'.$product->getName().'</div>';
        $barCode .= '</div>';

        return $this->render('admin/product/printBarCodes.html.twig', array(
            'barCode' => $barCode,
        ));
    }

    /**
     * @Route("backend/product/{id}", name="backend_product_show", methods={"GET"})
     */
    public function showAction(Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('admin/product/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("backend/product/{id}", name="backend_product_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Product $product)
    {
        $filter_form = $this->createDeleteForm($product);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush($product);

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_product_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}
