<?php

namespace App\Controller\Admin;

use App\Entity\RestrictedIP;
use App\Filter\RestrictedIPFilter;
use App\Form\RestrictedIPType;
use App\Repository\RestrictedIPRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class AdminRestrictedIPController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'restricted_ip';
    CONST ENTITY_NAME = 'RestrictedIP';
    CONST NS_ENTITY_NAME = 'App:RestrictedIP';

    /**
     * Lists all restricted_ip entities.
     *
     * @Route("backend/restricted_ip/index", name="backend_restricted_ip_index", methods={"GET"})
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, RestrictedIPFilter::class);

        return $this->render('admin/common/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'restricted_ip.id',
                    'sortable' => true,
                ],
                'a.ip_address' => [
                    'title' => 'IP addr',
                    'row_field' => 'ip_address',
                    'sorting_field' => 'restricted_ip.ip_address',
                    'sortable' => true,
                ],
            ]
        ));
    }


    /**
     * Creates a new restricted_ip entity.
     *
     * @Route("backend/restricted_ip/new", name="backend_restricted_ip_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, ValidatorInterface $validator)
    {
        $restricted_ip = new RestrictedIP();
        $form = $this->createForm(RestrictedIPType::class, $restricted_ip);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($restricted_ip);
            $em->flush();
            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_restricted_ip_edit', array('id' => $restricted_ip->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'row' => $restricted_ip,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    /**
     * Displays a form to edit an existing restricted_ip entity.
     *
     * @Route("backend/restricted_ip/{id}/edit", name="backend_restricted_ip_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, RestrictedIP $restricted_ip, FileUploader $fileUploader, EntityManagerInterface $em)
    {
        $deleteForm = $this->createDeleteForm($restricted_ip);
        $editForm = $this->createForm('App\Form\RestrictedIPType', $restricted_ip);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('backend_restricted_ip_edit', array('id' => $restricted_ip->getId()));
        }
        if ($editForm->isSubmitted() && !$editForm->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        return $this->render('admin/common/edit.html.twig', array(
            'row' => $restricted_ip,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Deletes a restricted_ip entity.
     *
     * @Route("backend/restricted_ip/{id}", name="backend_restricted_ip_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, RestrictedIP $restricted_ip)
    {
        $filter_form = $this->createDeleteForm($restricted_ip);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($restricted_ip);
            $em->flush();

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_restricted_ip_index');
    }

    /**
     * Creates a form to delete a restricted_ip entity.
     *
     * @param RestrictedIP $restricted_ip The restricted_ip entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(RestrictedIP $restricted_ip)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_restricted_ip_delete', array('id' => $restricted_ip->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


}
