<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Entity\ProductFilterValue;
use App\Entity\ProductGroup;
use App\Filter\ProductFilter;
use App\Form\ProductGroupType;
use App\Form\ProductType;
use App\Repository\CategoryRepository;
use App\Repository\ProductGroupRepository;
use App\Repository\ProductRepository;
use App\Utils\Slugger;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Service\FileUploader;

class AdminProductGroupController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'product_group';
    CONST ENTITY_NAME = 'ProductGroup';
    CONST NS_ENTITY_NAME = 'App:ProductGroup';

    /**
     * Lists all product entities.
     *
     * @Route("backend/product_group/index", name="backend_product_group_index", methods={"GET"})
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, ProductFilter::class);

        return $this->render('admin/product_group/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'list_fields' => [
                'a.id' => [
                    'title' => 'ID',
                    'row_field' => 'id',
                    'sorting_field' => 'productGroup.id',
                    'sortable' => true,
                ],
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'productGroup.name',
                    'sortable' => true,
                ],

                'a.pic' => [
                    'title' => 'Pic',
                    'row_field' => 'pic',
                    'sorting_field' => 'product.pic',
                    'sortable' => false,
                ],

            ]
        ));
    }


    /**
     * Creates a new product entity.
     *
     * @Route("backend/product_group/new", name="backend_product_group_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request,
                              ProductGroupRepository $repository,
                              FileUploader $fileUploader)
    {
        $productGroup = new ProductGroup();
        $form = $this->createForm(ProductGroupType::class, $productGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $productGroup->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $productGroup->setPic($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($productGroup);
            $em->flush();

            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_product_group_edit', array('id' => $productGroup->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'product' => $productGroup,
            'form' => $form->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,

        ));
    }

    private function makeSlug(Product $product, ProductRepository $repository)
    {
        $slug = Slugger::urlSlug(sprintf('%s %s',
            $product->getVendor()->getSlug(), $product->getName()), array('transliterate' => true)
        );

        while($repository->slugExists($slug)) {
            $slug .= '-' . rand(1000, 9999);
        }

        return $slug;
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/product_group/{id}/clone", name="backend_product_group_clone", methods={"GET"})
     */
    public function cloneAction(Request $request, Product $product, ProductRepository $repository)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Product $new_product */
        $new_product = clone $product;
        $new_product->setSlug($this->makeSlug($new_product, $repository));
        $em->persist($new_product);

        /** @var ProductFilterValue $productFilterValue */
        foreach ($product->getProductFilterValues() as $productFilterValue) {
            $newProductFilterValue = clone $productFilterValue;
            $newProductFilterValue->setProductId($new_product);
            $em->persist($newProductFilterValue);
        }

        $em->flush();

        $this->addFlash('success', 'Product was cloned successfully!');
        return $this->redirectToRoute('backend_product_group_edit', array('id' => $new_product->getId()));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("backend/product_group/{id}/edit", name="backend_product_group_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, 
                               ProductGroup $row,
                               ProductGroupRepository $repository,
                               FileUploader $fileUploader,
                               EntityManagerInterface $em
    )
    {
        $originalProducts = new ArrayCollection();

        // Create an ArrayCollection of the current Tag objects in the database
        foreach ($row->getProducts() as $product) {
            $originalProducts->add($product);
        }

        $form = $this->createForm(ProductGroupType::class, $row);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $row->getPicFile();

            if (null !== $file) {
                $fileName = $fileUploader->upload($file);
                $row->setPic($fileName);
            }

            $em->persist($row);
            $em->flush();

            $this->addFlash('success', 'Your changes were saved!');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }

        $deleteForm = $this->createDeleteForm($row);

        return $this->render('admin/product_group/edit.html.twig', array(
            'row' => $row,
//            'productFilters' => $productFilters,
//            'categoryFilters' => $categoryFilters,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("backend/product_group/{id}", name="backend_product_group_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Product $product)
    {
        $filter_form = $this->createDeleteForm($product);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush($product);

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_product_group_index');
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Product $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProductGroup $row)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_product_group_delete', array('id' => $row->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
