<?php


namespace App\Controller\Admin;

use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("backend/product/import/xml")
 */
class AdminImportXmlController
{
    private const CRAZY_FISH_URL = 'https://crazy-fish.biz/bitrix/catalog_export/partners.xml';
    /**
     * @Route("/crazy_fish", name="backend_import_xm_crazy_fish", methods={"GET"})
     */
    public function importCrazyFish(CategoryRepository $categoryRepository, Request $request,
                                    ProductRepository $productRepository): Response
    {

        $stream = fopen(self::CRAZY_FISH_URL, 'r');
        $parser = xml_parser_create();

        $xmlstr = implode("\n", file(self::CRAZY_FISH_URL));
        $rows = new \SimpleXMLElement($xmlstr);

        while (($data = fread($stream, 16384))) {
            xml_parse($parser, $data); // parse the current chunk
        }

        xml_parse($parser, '', true); // finalize parsing
        xml_parser_free($parser);
        fclose($stream);

    }

}