<?php

namespace App\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    /**
     * @Route("/backend/data", name="backend_data_example")
     */
    public function dataExample()
    {
        return $this->render('admin/table_example.html.twig', [
        ]);
    }

}
