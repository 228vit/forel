<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Filter\OrderFilter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Annotation\Route;


class AdminOrderController extends AbstractController
{
    use AdminTraitController;

    CONST ROWS_PER_PAGE = 10;
    CONST MODEL = 'orders';
    CONST ENTITY_NAME = 'Order';
    CONST NS_ENTITY_NAME = 'App:Order';

    /**
     * Lists all order entities.
     *
     * @Route("backend/order/index", name="backend_order_index", methods={"GET"})
     */
    public function indexAction(Request $request, SessionInterface $session)
    {
        $pagination = $this->getPagination($request, $session, OrderFilter::class);


        return $this->render('admin/order/index.html.twig', array(
            'pagination' => $pagination,
            'current_filters' => $this->current_filters,
            'filter_form' => $this->filter_form->createView(),
            'model' => self::MODEL,
        ));
    }


    /**
     * Creates a new order entity.
     *
     * @Route("backend/order/new", name="backend_order_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, ValidatorInterface $validator)
    {
        $order = new Order();
        $form = $this->createForm('App\Form\OrderType', $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $em->persist($order);
            $em->flush($order);
            $this->addFlash('success', 'New record was created!');

            return $this->redirectToRoute('backend_order_edit', array('id' => $order->getId()));
        }
        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('danger', 'Errors due creating object!');
        }

        return $this->render('admin/common/new.html.twig', array(
            'order' => $order,
            'form' => $form->createView(),
            'model' => 'order',
            'entity_name' => 'Order'
        ));
    }

    /**
     * Finds and displays a order entity.
     *
     * @Route("backend/order/{id}", name="backend_order_show",  methods={"GET"})
     */
    public function showAction(Order $order)
    {
        $deleteForm = $this->createDeleteForm($order);

        return $this->render('admin/order/show.html.twig', array(
            'order' => $order,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing order entity.
     *
     * @Route("backend/order/{id}/edit", name="backend_order_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Order $order)
    {
        $deleteForm = $this->createDeleteForm($order);
        $editForm = $this->createForm('App\Form\OrderType', $order);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('backend_order_edit', array('id' => $order->getId()));
        }

        if ($editForm->isSubmitted() && !$editForm->isValid()) {
            $this->addFlash('danger', 'Errors due saving object!');
        }


        return $this->render('admin/order/edit.html.twig', array(
            'row' => $order,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'model' => 'order', // self::MODEL,
            'entity_name' => 'Order'
        ));
    }

    /**
     * Deletes a order entity.
     *
     * @Route("backend/order/{id}", name="backend_order_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Order $order)
    {
        $filter_form = $this->createDeleteForm($order);
        $filter_form->handleRequest($request);

        if ($filter_form->isSubmitted() && $filter_form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($order);
            $em->flush($order);

            $this->addFlash('success', 'Record was successfully deleted!');
        }

        if (!$filter_form->isValid()) {
            /** @var FormErrorIterator $errors */
            $errors = $filter_form->getErrors()->__toString();
            $this->addFlash('danger', 'Error due deletion! ' . $errors);
        }

        return $this->redirectToRoute('backend_order_index');
    }

    /**
     * Creates a form to delete a order entity.
     *
     * @param Order $order The order entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Order $order)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_order_delete', array('id' => $order->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

}
