<?php

namespace App\Controller\Frontend;

use App\Entity\Feedback;
use App\Form\FeedbackType;
use App\Utils\reCaptcha;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FeedbackController extends Controller
{
    /**
     * @Route("/feedback/success", name="feedback_success")
     * @Method("GET")
     */
    public function success(Request $request)
    {
        return $this->render('feedback/success.html.twig', [
        ]);
    }

    public function shortFeedbackForm()
    {
        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);

        return $this->render('feedback/footer_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/feedback/create", name="feedback_create")
     * @Method("POST")
     */
    public function create(Request $request, \Swift_Mailer $mailer)
    {
        $isAjax = $request->isXmlHttpRequest();

        if (!$isAjax) {}

        $feedback = new Feedback();
        $form = $this->createForm(FeedbackType::class, $feedback);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { //  && reCaptcha::check()

            $em = $this->getDoctrine()->getManager();

            $em->persist($feedback);
            $em->flush($feedback);

            // todo: notice admin by email!!!
            $message = (new \Swift_Message('new feedback #' . $feedback->getId()))
                ->setFrom('noreply@na-forel.ru')
                ->setTo('info@na-forel.ru')
                ->setBody($feedback->getMessage(),'text/plain')
            ;

            $success_cnt = $mailer->send($message);

            $this->addFlash('success','Feedback success!');

            return $isAjax ?
                new JsonResponse(['status' => 'success']) :
                new RedirectResponse($this->generateUrl('feedback_success'));
        }

        $this->addFlash('danger','Ошибка при отправке формы.');

        return $isAjax ?
            // todo: extract form errors
            new JsonResponse(['status' => 'fail'], 400) :
            $this->render('feedback/new.html.twig', [
                'form' => $form->createView(),
            ])
        ;
    }

}
