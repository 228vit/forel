<?php

namespace App\Controller\Frontend;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ProductController extends Controller
{
    use FrontendTraitController;

    /**
     * @Route("/category/{categorySlug}/{productSlug}", name="product_view")
     * @Method({"GET", "POST"})
     */
    public function viewProduct(Request $request,
                                ProductRepository $productRepository,
                                CategoryRepository $categoryRepository,
                                $categorySlug, $productSlug)
    {
        /** @var Product $product */
        $product = $productRepository->findOneBy(['slug' => $productSlug]);

        /** @var Category $category */
        $category = $categoryRepository->findOneBy(['slug' => $categorySlug]);
        if (!isset($product, $category)) {
            throw new NotFoundHttpException();
        }

        $parentCategory = $category->getParent();
        $similarProducts = $productRepository->similarProducts($category, $product);

        return $this->render('product/show.html.twig', [
            'category' => $category,
            'parentCategory' => $parentCategory,
            'product' => $product,
            'similarProducts' => $similarProducts,
        ]);
    }

    /**
     * @Route("/category/{slug}/id/{id}", name="product_show")
     * @Method({"GET", "POST"})
     */
    public function showProduct(Request $request, ProductRepository $productRepository,
                                CategoryRepository $categoryRepository, $slug, $id)
    {
        $category = $categoryRepository->findOneBy(['slug' => $slug]);

        /** @var Product $product */
        $product = $productRepository->find($id);

        if (!$product) {
            throw new NotFoundHttpException();
        }

        return new RedirectResponse($this->generateUrl('product_view', [
            'categorySlug' => $slug,
            'productSlug' => $product->getSlug()
        ]));
    }

    /**
     * Генерация витрины товаров на главной стр.
     * @return Response
     */
    public function showOnTop(ProductRepository $repository)
    {
        $rows = $repository->findByShowOnTop();

        return $this->render('product/list.html.twig', [
            'rows' => $rows
        ]);
    }

}