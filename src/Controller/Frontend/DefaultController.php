<?php

namespace App\Controller\Frontend;

use App\Repository\ProductRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->render('homepage/index.html.twig', [
        ]);
    }

    /**
     * @Route("/sale", name="sale")
     */
    public function sale(ProductRepository $repository)
    {
        return $this->render('homepage/sale.html.twig', [
            'rows' => $repository->sale(),
        ]);
    }

    /**
     * @Route("/msklad", name="msklad")
     */
    public function msklad(ProductRepository $repository)
    {
        $apiSettings['login'] = 'admin@228vit';
        $apiSettings['password'] = '3854aba5f9';
        $apiSettings['user_agent'] = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:107.0) Gecko/20100101 Firefox/107.0";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);

        $userName = $apiSettings['login'];
        $userPassword = $apiSettings['password'];
        curl_setopt($curl, CURLOPT_USERPWD, "$userName:$userPassword");
        curl_setopt($curl, CURLOPT_USERAGENT, $apiSettings['user_agent']);

        $response = curl_exec($curl);

        die($response);

        return $this->render('homepage/sale.html.twig', [
            'rows' => $repository->sale(),
        ]);
    }

}
