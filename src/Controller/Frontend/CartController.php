<?php

namespace App\Controller\Frontend;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Repository\OrderRepository;
use App\Repository\RestrictedIPRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;

class CartController extends Controller
{
    /**
     * @Route("/cart/add/{product_id}", name="add_to_cart")
     */
    public function addToCart(Request $request, $product_id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Product $product */
        $product = $em->getRepository('App:Product')->findOneBy(['id' => $product_id]);

        if (!$product) {
            throw new NotFoundHttpException();
        }

        $session = $this->get('session');
        $cart = $session->get('cart');
        if (isset($cart[$product_id])) {
            $cart[$product_id]++;
        } else {
            $cart[$product_id] = 1;
        }

        $session->set('cart', $cart);
        $session->getFlashBag()->add('success', $product->getName() . ' добавлен в Вашу корзину.');

        $referer = $request->headers->get('referer');

        $response = new RedirectResponse($referer);
        $response->headers->setCookie(Cookie::create('cart', serialize($cart)));

        return $response;
    }

    /**
     * @Route("/cart/update/{mode}/{product_id}", name="cart_item_update")
     */
    public function updateCartItem(Request $request, $mode, $product_id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Product $product */
        $product = $em->getRepository('App:Product')->findOneBy(['id' => $product_id]);

        if (!$product) {
            throw new NotFoundHttpException();
        }

        $session = $this->get('session');
        $cart = $session->get('cart');
        if (isset($cart[$product_id])) {
            if ($mode == 'increase') {
                $cart[$product_id]++;
            } elseif ($mode == 'decrease') {
                $cart[$product_id]--;
                if ($cart[$product_id] <= 0) {
                    unset($cart[$product_id]);
                }
            }
        }

        $session->set('cart', $cart);
        $session->getFlashBag()->add('success', 'Ваша корзина обновлена.');

        $referer = $request->headers->get('referer');

        $response = new RedirectResponse($referer);
        $response->headers->setCookie(Cookie::create('cart', serialize($cart)));

        return $response;
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @Route("/cart/remove/{id}", name="cart_remove_item")
     */
    public function removeFromCartAction(Request $request, $id)
    {
        $session = $this->get('session');
        $cart = $session->get('cart', false);

        if (is_array($cart) && ($cart[$id])) {
            unset($cart[$id]);
            $session->set('cart', $cart);
            $this->addFlash('success', 'Товар удалён из корзины');
        } else {
            $this->addFlash('error', 'Неверный параметр');
        }

        $referer = $request->headers->get('referer');
        $response = new RedirectResponse($referer);
        $response->headers->setCookie(Cookie::create('cart', serialize($cart)));

        return $response;
    }

    /**
     * @Route("/cart/show", name="cart_show")
     */
//    public function showCart(Request $request)
//    {
//        $session = $this->get('session');
//        $cart = $session->get('cart');
//
//        return $this->render('cart/checkout.html.twig', [
//            'cart' => $cart_arr,
//            'total' => $total,
//            'server_name' => sprintf('%s://%s', $_SERVER['REQUEST_SCHEME'], $_SERVER['SERVER_NAME']),
//        ]);
//
//    }

    public function showCartDropDown()
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        $cart = $session->get('cart');
        $product_ids = is_array($cart) ? array_keys($cart) : array();
        $cart_total_qty = 0;
        $cart_total_sum = 0;

        $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
        $query->setParameter('ids', $product_ids);
        $products = $query->getResult();

        $cart_arr = [];

        /** @var Product $product */
        foreach ($products as $product) {
            $cart_item_qty = $cart[$product->getId()];
            $cart_total_qty += $cart_item_qty;
            $cart_total_sum += $product->getPrice() * $cart_item_qty;

            // todo: make a class Cart, not an array
            $cart_arr[$product->getId()]['product'] = array(
                'id' => $product->getId(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'pic' => $product->getPic(),
            );

            $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
        }

        return $this->render('cart/cart_as_dropdown.html.twig', [
            'cart' => $cart_arr,
            'cart_total_qty' => $cart_total_qty,
            'cart_total_sum' => $cart_total_sum,
        ]);
    }

    public function newCartDropDown()
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        $cart = $session->get('cart');
        $product_ids = is_array($cart) ? array_keys($cart) : array();
        $cart_total_qty = 0;
        $cart_total_sum = 0;

        $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
        $query->setParameter('ids', $product_ids);
        $products = $query->getResult();

        $cart_arr = [];

        /** @var Product $product */
        foreach ($products as $product) {
            $cart_item_qty = $cart[$product->getId()];
            $cart_total_qty += $cart_item_qty;
            $cart_total_sum += $product->getPrice() * $cart_item_qty;

            // todo: make a class Cart, not an array
            $cart_arr[$product->getId()]['product'] = array(
                'id' => $product->getId(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'pic' => $product->getPic(),
            );

            $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
        }

        return $this->render('cart/new_cart_as_dropdown.html.twig', [
            'cart' => $cart_arr,
            'cart_total_qty' => $cart_total_qty,
            'cart_total_sum' => $cart_total_sum,
        ]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     * @Route("/buy/{id}", name="buy_in_one_click")
     */
    public function buyOneClickAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Product $product */
        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            throw new NotFoundHttpException();
        }

        // todo: может без корзины пересылать на форму оформления?
        $session = $this->get('session');
        $cart = $session->get('cart');
        $cart[$id] = 1;
//        if (isset($cart[$id])) {
//            $cart[$id] = 1;
//        } else {
//            throw new NotFoundHttpException();
//        }
        $session->set('cart', $cart);
        $response = new RedirectResponse($this->generateUrl('checkout'));
        $response->headers->setCookie(Cookie::create('cart', serialize($cart)));

        return $response;
    }

    /**
     * Отправить корзину по почте
     *
     * @param Request $request
     * @Route("/checkout/mail", name="checkout_mail")
     */
    public function checkoutMailAction(Request $request, \Swift_Mailer $mailer)
    {
        $session = $this->get('session');
        $cart = $session->get('cart', false);
        if (!$cart) {
            return new JsonResponse([], 400);
        }
        $product_ids = array_keys($cart);

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
        $query->setParameter('ids', $product_ids);
        $products = $query->getResult();

        $cart_arr = [];
        $total = 0;
        /** @var Product $product */
        foreach ($products as $product) {
            // todo: make a class Cart not an array
            $cart_arr[$product->getId()]['product'] = $product;
            $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
            $cart_arr[$product->getId()]['amount'] = $amount = round($cart[$product->getId()] * $product->getPrice(), 2);
            $total += $amount;
        }

        if ($total > 1000 AND $total < 5000) {
            $discountPercent = 3;
        } else if ($total >= 5000 AND $total < 10000) {
            $discountPercent = 5;
        } else if ($total >= 10000) {
            $discountPercent = 10;
        } else {
            $discountPercent = 0;
        }
        $discountPercent = 0;

        $discountAmount = ($discountPercent > 0) ? round(($total/100) * $discountPercent, 2) : 0;

        $message = (new \Swift_Message('checkout'))
            ->setFrom('noreply@na-forel.ru')
            ->setTo('228vit@gmail.com')
            ->setBody(
                $this->renderView('cart/mail_new_checkout.html.twig',
                    array(
                        'ip' => $request->getClientIp(),
                        'browser' => $_SERVER['HTTP_USER_AGENT'],
                        'cart' => $cart_arr,
                        'total' => $total,
                        'discountPercent' => $discountPercent,
                        'discountAmount' => $discountAmount,
                    )
                ),
                'text/html'
            )
        ;
        $success_cnt = $mailer->send($message);

        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse | Response
     * @Route("/checkout", name="checkout")
     */
    public function checkoutAction(Request $request,
                                   \Swift_Mailer $mailer,
                                   RestrictedIPRepository $repository)
    {
        $session = $this->get('session');
        $cart = $session->get('cart', false);

        $cart = unserialize($request->cookies->get('cart', null));

        if (!$cart) {
            $session->getFlashBag()->add('success', 'Ваша корзина пуста');

            // todo: AJAX request and JSON response

            $referer = $request->headers->get('referer');
            if (strpos('/cart/show', $referer)) {
                $session->getFlashBag()->add('success', 'Ваша корзина пуста');
                return new RedirectResponse($referer);
            } else {
                return $this->redirectToRoute('homepage');
            }
        }

        $product_ids = array_keys($cart);

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
        $query->setParameter('ids', $product_ids);
        $products = $query->getResult();

        $cart_arr = [];
        $total = 0;
        /** @var Product $product */
        foreach ($products as $product) {
            // todo: make a class Cart not an array
            $cart_arr[$product->getId()]['product'] = $product;
            $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
            $cart_arr[$product->getId()]['amount'] = $amount = round($cart[$product->getId()] * $product->getPrice(), 2);
            $total += $amount;
        }

        if ($total > 1000 AND $total < 5000) {
            $discountPercent = 3;
        } else if ($total >= 5000 AND $total < 10000) {
            $discountPercent = 5;
        } else if ($total >= 10000) {
            $discountPercent = 10;
        } else {
            $discountPercent = 0;
        }
        $discountPercent = 0;
        $discountAmount = ($discountPercent > 0) ? round(($total/100) * $discountPercent, 2) : 0;

        // todo: filter by IPs
        $isIpResticted = null; //$repository->ipStartsWith($request->getClientIp());

        if (null !== $isIpResticted) {
            $message = (new \Swift_Message('checkout'))
                ->setFrom('noreply@na-forel.ru')
                ->setTo('228vit@gmail.com')
                ->setBody(
                    $this->renderView('cart/mail_new_checkout.html.twig',
                        array(
                            'ip' => $request->getClientIp(),
                            'browser' => $_SERVER['HTTP_USER_AGENT'],
                            'cart' => $cart_arr,
                            'total' => $total,
                            'discountPercent' => $discountPercent,
                            'discountAmount' => $discountAmount,
                        )
                    ),
                    'text/html'
                )
            ;
            $mailer->send($message);
        }

        return $this->render('cart/checkout.html.twig', [
            'cart' => $cart_arr,
            'discountPercent' => $discountPercent,
            'discountAmount' => $discountAmount,
            'total' => $total,
            'server_name' => sprintf('%s://%s', $_SERVER['REQUEST_SCHEME'], $_SERVER['SERVER_NAME']),
        ]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     * @Route("/order", name="place_order")
     */
    public function orderAction(Request $request, \Swift_Mailer $mailer)
    {
        try {
            $cart = $this->checkNotEmptyShoppingCart($request);

            if (null === $cart) {
                $this->addFlash('error', 'Ваша корзина пуста!');
                return new RedirectResponse($this->generateUrl('homepage'));
            }

            $product_ids = array_keys($cart);

            $em = $this->getDoctrine()->getManager();

            $phone = $request->request->get('phone');

            $email = $request->request->get('email');
            if ('testing@example.com' === $email) {
                return new Response('Bad request');
            }

            // create order
            $order = new Order();
            // todo: use form for validation
            $order->setName(strip_tags($request->request->get('name')));
            $order->setEmail(strip_tags($email));
            $order->setPhone($phone);
            $order->setAddress(strip_tags($request->request->get('address')));
            $order->setNote(strip_tags($request->request->get('note')));
            $order->setDeliveryType($request->request->get('delivery_type'));
            $order->setAddress(strip_tags($request->request->get('address')));
            $order->setStatus(Order::STATUS_NEW);
            $em->persist($order);
            $em->flush();

            $cart_arr = [];
            $total = 0;

            $query = $em->createQuery('SELECT p, c FROM App:Product p INNER JOIN p.category c WHERE p.id IN (:ids)');
            $query->setParameter('ids', $product_ids);
            $products = $query->getResult();

            /** @var Product $product */
            foreach ($products as $product) {
                $order_item = new OrderItem();
                // todo: make setOrder
                $order_item->setOrder($order);
                $order_item->setProduct($product);
                $order_item->setPrice($product->getPrice());
                $order_item->setQty($cart[$product->getId()]);
                $em->persist($order_item);

                // todo: make a class Cart not an array
                $cart_arr[$product->getId()]['product'] = $product;
                $cart_arr[$product->getId()]['qty'] = $cart[$product->getId()];
                $cart_arr[$product->getId()]['amount'] = $amount = round($cart[$product->getId()] * $product->getPrice(), 2);
                $total += $amount;
            }

            $em->flush();

            if ($total > 1000 AND $total < 5000) {
                $discountPercent = 3;
            } else if ($total >= 5000 AND $total < 10000) {
                $discountPercent = 5;
            } else if ($total >= 10000) {
                $discountPercent = 10;
            } else {
                $discountPercent = 0;
            }
            $discountPercent = 0;
            $discountAmount = ($discountPercent > 0) ? round(($total / 100) * $discountPercent, 2) : 0;

            $order->setDiscountPercent($discountPercent)
                ->setDiscountAmount($discountAmount);
            $em->persist($order);
            $em->flush();

            $message = (new \Swift_Message('new order #' . $order->getId()))
                ->setFrom('noreply@na-forel.ru')
                ->setTo(['info@na-forel.ru', '228vit@gmail.com'])
                ->setReplyTo($order->getEmail())
                ->setBody(
                    $this->renderView('cart/mail_new_order_to_admin.html.twig',
                        array(
                            'order' => $order,
                            'cart' => $cart_arr,
                            'total' => $total,
                            'discountPercent' => $discountPercent,
                            'discountAmount' => $discountAmount,
                            'IP' => $request->getClientIp(),
                        )
                    ),
                    'text/html'
                );
            $userMessage = (new \Swift_Message('[na-forel.ru] заказ #' . $order->getId()))
                ->setFrom('noreply@na-forel.ru')
                ->setTo($order->getEmail())
                ->setReplyTo('info@na-forel.ru')
                ->setBody(
                    $this->renderView('cart/mail_new_order.html.twig',
                        array(
                            'order' => $order,
                            'cart' => $cart_arr,
                            'total' => $total,
                            'discountPercent' => $discountPercent,
                            'discountAmount' => $discountAmount,
                        )
                    ),
                    'text/html'
                );

            $mailer->send($message);
            $mailer->send($userMessage);

            // clear cart!!!
            $session = $this->get('session');
            $session->set('cart', false);
        } catch (\Exception $e) {
            $userMessage = (new \Swift_Message('[na-forel.ru] order error at' . date('Y-m-d- h:i:s')))
                ->setFrom('noreply@na-forel.ru')
                ->setTo($order->getEmail())
                ->setReplyTo('info@na-forel.ru')
                ->setBody("
<p>Code: {$e->getCode()}</p>
<p>Message: {$e->getMessage()}</p>
<p>File: {$e->getLine()}</p>
<p>Line: {$e->getLine()}</p>
<pre>Trace: {$e->getTraceAsString()}</pre>
",
                    'text/html'
                );

            $mailer->send($message);
            $mailer->send($userMessage);
        }
        $url = $this->generateUrl('order_success', ['uuid' => $order->getUuid()]);
        $response = new RedirectResponse($url);
        $response->headers->setCookie(Cookie::create('cart', null));

        return $response;
    }

    /**
     * @Route("/order/success/{uuid}", name="order_success")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function orderSuccess(Request $request, $uuid)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var OrderRepository $repo */
        $repo = $em->getRepository(Order::class);

        $order = $repo->findOneBy(['uuid' => $uuid]);

        if (!$order) {
            $this->addFlash('error','Неверный параметр');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('cart/order_success.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/cart/delete/item", name="cart_delete_item")
     */
    public function cartDeleteItemAction(Request $request)
    {
        $id = $request->query->get('id');
        $session = $this->get('session');
        $cart = $session->get('cart');

        unset($cart[$id]);

        $session->set('cart', $cart);
        $session->getFlashBag()->add('success', 'Ваша корзина обновлена');

        $response = new JsonResponse(['status' => 'ok', 'cart_items_cnt' => count($cart)]);
        $response->headers->setCookie(Cookie::create('cart', serialize($cart)));

        return $response;
    }

    public function renderCart(Request $request)
    {
        $session = $this->get('session');
        $cart = $session->get('cart');
        $request->cookies->get('cart');

        return $this->render('cart/cart_in_header.html.twig', [
            'rows' => $cart
        ]);
    }

    /**
     * @param Request $request
     * @return array|null
     */
    private function checkNotEmptySHoppingCart(Request $request) : ?array
    {
        $session = $this->get('session');
        $cart = $session->get('cart', null);
        if (null === $cart) {
            $cart = unserialize($request->cookies->get('cart', null));
        }

        return $cart;
    }
}
