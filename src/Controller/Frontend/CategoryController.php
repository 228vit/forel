<?php

namespace App\Controller\Frontend;

use App\Entity\Category;
use App\Entity\Product;
use App\Filter\ProductFilter;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\VendorRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class CategoryController extends Controller
{
    use FrontendTraitController;

    CONST ROWS_PER_PAGE = 24;
    CONST MODEL = 'product';
    CONST ENTITY_NAME = 'Product';
    CONST NS_ENTITY_NAME = 'App:Product';

    /**
     * Генерация меню в каталожном LAYOUT
     * @return Response
     */
    public function categoriesTreeMenu()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $products_count = $em->createQueryBuilder()
            ->select('c.id, COUNT(p.id) as prod_cnt')
            ->from('App:Category', 'c')
            ->innerJoin('c.products', 'p')
            ->where('p.isActive = :isActive')
            ->setParameter('isActive', true)
            ->groupBy('c.id')
            ->getQuery()
            ->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        $category_product_counts = [];

        foreach ($products_count as $item) {
            $category_product_counts[$item['id']] = $item['prod_cnt'];
        }

        $repo = $em->getRepository('App:Category');
        $arrayTree = $repo->childrenHierarchy();

        $tree_arr = [];
        foreach ($arrayTree as $index => $node) {
            if (false === $node['isActive']) {
                continue;
            }
            $tree_arr[$index] = [
                'name' => $node['name'], // . $node['id'],
                'href' => $url = $this->generateUrl("category_show", array(
                        "slug" => $node['slug'])
                ),
            ];
            $tree_arr[$index]['products_cnt'] = isset($category_product_counts[$node['id']]) ? $category_product_counts[$node['id']] : '';
            if (is_array($node['__children']) && sizeof($node['__children'])) {
                $tree_arr[$index]['children'] = $this->renderSubtree($node['__children'], $category_product_counts);
            }
        }

        return $this->render('common/tree_as_menu.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            'array_tree' => $arrayTree,
            'tree_arr' => $tree_arr,
            'products_count' => $products_count,
        ]);
    }

    private function renderSubtree($arrayTree, $category_product_counts)
    {
        $tree_arr = [];
        foreach ($arrayTree as $index => $node) {
            $tree_arr[] = [
                'name' => $node['name'], // . $node['id'],
                'href' => $url = $this->generateUrl("category_show", array(
                        "slug" => $node['slug'])
                ),
            ];
            $tree_arr[$index]['products_cnt'] = isset($category_product_counts[$node['id']]) ? $category_product_counts[$node['id']] : '';
            if (is_array($node['__children']) && sizeof($node['__children'])) {
                $tree_arr[$index]['children'] = $this->renderSubtree($node['__children'], $category_product_counts);
            }
        }

        return $tree_arr;
    }

    public function buildNewQuery(ProductRepository $repository, Request $request, Category $category): QueryBuilder
    {
        $checkedVendors = $request->query->get('vendor', []);
        $categoryFilters = $request->query->get('cfilter', []);
        $onStock = boolval($request->query->get('stock', false));

        $queryBuilder = $repository->createQueryBuilder('p')
            ->innerJoin('p.vendor', 'v')
            ->innerJoin('p.category', 'c')
            ->leftJoin('p.productFilterValues', 'pfv')
            ->where('p.isActive = true')
            ->andWhere('p.category = :category')
            ->andWhere('p.category = :category')
            ->setParameter('category', $category->getId())
        ;

        if ($onStock) {
            $queryBuilder->andWhere('p.status = :stock')
                ->setParameter('stock', Product::STATUS_STOCK);
        }

        if ([] !== $checkedVendors) {
            $queryBuilder->andWhere('p.vendor IN (:vendors)')
                ->setParameter('vendors', $checkedVendors);
        }

        if ([] !== $categoryFilters) {
//            foreach ($categoryFilters as $cfId => $categoryFilter) {
//                $queryBuilder->andWhere("pfv.category_filter_value_id IN (:filter_values_{$cfId})")
//                    ->setParameter("filter_values_{$cfId}", array_values($categoryFilter));
//            }
            
            $currentFilters = [];
            array_walk_recursive($categoryFilters, function ($value) use (&$currentFilters) {
                $currentFilters[] = $value;
            });

            $queryBuilder->andWhere("pfv.categoryFilterValue IN (:filter_values)")
                ->setParameter("filter_values", $currentFilters);
            $queryBuilder->groupBy('p.id');
            $queryBuilder->having('COUNT(pfv.id) = ' . count($categoryFilters));
        }

        return $queryBuilder;
    }

    private function getFilteredProductIds(QueryBuilder $query)
    {
        $rows = $query->select('DISTINCT p.id')
            ->getQuery()->getResult()
        ;

        $ids = array_map(function ($row) {
            return $row['id'];
        }, $rows);

        return array_values($ids);
    }

    private function getQueryForPager(ProductRepository $repository,
                                      QueryBuilder $queryBuilder, array $ids): Query
    {

        $queryBuilder = $repository->createQueryBuilder('p')
            ->innerJoin('p.vendor', 'v')
            ->where('p.isActive = true')
            ->andWhere('p.id IN (:ids)')
            ->setParameter('ids', $ids)
        ;

        return $queryBuilder->getQuery();
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/category/{slug}", name="category_show")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param $slug
     * @return Response
     */
    public function showCategory(Request $request,
                                 SessionInterface $session,
                                 CategoryRepository $repository,
                                 ProductRepository $productRepository,
                                 VendorRepository $vendorRepository,
                                 string $slug)
    {
        /** @var Category $category */
        $category = $repository->findOneBy(['slug' => $slug]);
        if (!$category) {
            throw new NotFoundHttpException();
        }

        $parentCategory = $category->getParent();

        // add to session filter category
        $session_filters = $session->get('filters', array());
        $filter = @$session_filters[self::MODEL];

        $checkedVendors = $request->query->get('vendor', []);
        $filter['stock'] = boolval($request->query->get('stock', false));

        if ([] !== $checkedVendors) {
            $filter['vendor'] = $checkedVendors;
        }

        $filter['category'] = $category->getId();

        /** @var QueryBuilder $query */
        $queryBuilder = $this->buildNewQuery($productRepository, $request, $category);

        $productIds = $this->getFilteredProductIds($queryBuilder);

        $query = $this->getQueryForPager($productRepository, $queryBuilder, $productIds);

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            self::ROWS_PER_PAGE
        );

        $categoryVendors = $vendorRepository->getCategoryVendors($category);
        $cfilters = $request->query->get('cfilter', []);
        $currentFilters = [];
        array_walk_recursive($cfilters, function ($value) use (&$currentFilters) {
            $currentFilters[] = $value;
        });

        $cfilter = $request->query->get('cfilter', false);
        $sort = $request->query->get('sort', false);
        $vendor = $request->query->get('vendor', false);

        $isPager = ($sort OR $vendor OR $cfilter) ? true : false;

        return $this->render('catalog/products.html.twig', array(
            'category' => $category,
            'categoryVendors' => $categoryVendors,
            'checkedVendors' => $checkedVendors,
            'parentCategory' => $parentCategory,
            'pagination' => $pagination,
            'cfilters' => $currentFilters,
            'current_filters' => false, // todo: remove it
            'model' => self::MODEL,
            'entity_name' => self::ENTITY_NAME,
            'slug' => $slug,
            'isPager' => $isPager,

            'list_fields' => [
                'a.name' => [
                    'title' => 'Name',
                    'row_field' => 'name',
                    'sorting_field' => 'product.name',
                    'sortable' => true,
                ],
                'a.price' => [
                    'title' => 'Price',
                    'row_field' => 'price',
                    'sorting_field' => 'product.price',
                    'sortable' => true,
                ],

            ]
        ));
    }
}