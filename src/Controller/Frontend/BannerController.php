<?php

namespace App\Controller\Frontend;

use App\Entity\Banner;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BannerController extends Controller
{

    /**
     * @Route("/banner/click/{id}", name="banner_click")
     */
    public function clickAction(Request $request, Banner $banner)
    {
        $em = $this->getDoctrine()->getManager();

        $banner->addClick();
        $em->persist($banner);
        $em->flush();

        return $this->redirect($banner->getUrl());
    }

    public function showHomepageBanner()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Banner $banner */
        $banner = $em->getRepository(Banner::class)->findOneBy(['isActive' => true]);

        if ($banner) {
            $banner->addShows();
            $em->persist($banner);
            $em->flush();

            return $this->render('banner/view.html.twig', array(
                'banner' => $banner,
            ));
        }

        return new Response('');
    }

    /**
     * @Route("/banner/{id}", name="banner_view")
     */
    public function bannerViewAction($id, Request $request)
    {
        return $this->viewAction($id);
    }

    private function viewAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $banner = $em->getRepository(Banner::class)->findOneBy(['id' => $id]);

        if (!$banner) {
            throw $this->createNotFoundException('The banner not exist');
        }

        return $this->render('banner/view.html.twig', array(
            'banner' => $banner,
        ));
    }

}