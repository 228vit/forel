<?php


namespace App\Controller\Frontend;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="front_search")
     * @Method({"GET", "POST"})
     */
    public function searchAction(ProductRepository $productRepository, Request $request)
    {
        if (null === $query = $request->query->get('q', null)) {
            $this->addFlash('alert', 'Пустой поиск.');
            return $this->redirectToRoute('homepage');
        }

        $onStock = $request->query->get('stock', false);

        $products = $productRepository->search($query, (bool)$onStock);

        return $this->render('common/search.html.twig', [
            'search' => $query,
            'products' => $products,
        ]);
    }
}