<?php

namespace App\Controller\Frontend;

use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ExceptionController extends Controller
{
    public function showException(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        $statusCode = $exception->getStatusCode(); // 404, 500..
        $path = $request->getPathInfo();

        $myadmins = [
            '/admin',
            '/phpmyadmin',
            '/pma',
            '/mysql',
            '/typo3',
            '/phpma',
            '/dbadmin',
            '/db',
            '/wp-',
        ];

        $hack = false;
        foreach ($myadmins as $hack_str) {
            $pos = stristr($path, $hack_str);
            if ($pos !== false) {
                $hack = true;
                break;
            }
        }

        if ($hack) {
            return $this->render('error/hack.html.twig');
        }

        if ($statusCode == '404') {
            return $this->render('error/error404.html.twig');
        }

        return $this->render('error/error.html.twig', [
            'code' => $statusCode,
            'message' => $exception->getMessage(),
            'trace' => $exception->getTrace(),
        ]);


    }

}
