<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\CategoryFilter;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name')
            ->add('singleName', TextType::class, [
                'label' => 'В ед.числе'
            ])
            ->add('pluralName', TextType::class, [
                'label' => 'Во мн.числе'
            ])
            ->add('slug')
            ->add('meta_description')
            ->add('root')
            ->add('parent')
            ->add('isActive')
//            ->add('filters', EntityType::class, array(
//                'class' => CategoryFilter::class,
//                'query_builder' => function (EntityRepository $er) {
//                    return $er->createQueryBuilder('c')
//                        ->addOrderBy('c.name', 'ASC');
//                },
////                'choice_label' => 'forTree',
//                'expanded' => false,
//            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_category';
    }


}
