<?php

namespace App\Form;

use App\Entity\Banner;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class BannerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type', ChoiceType::class, [
                'choices'  => [
                    Banner::TYPE_PIC => Banner::TYPE_PIC,
                    Banner::TYPE_TEXT => Banner::TYPE_TEXT,
                    Banner::TYPE_COMBINED => Banner::TYPE_COMBINED,
                ],
                'expanded' => true,
            ])
            ->add('url')
            ->add('shows')
            ->add('clicks')
            ->add('content', TextareaType::class)
//            ->add('answer', CKEditorType::class, [
//                'config' => [
//                    'uiColor' => '#ffffff',
//                    //...
//                ],
//            ])
            ->add('picFile', FileType::class, array(
                'label' => 'Image file',
                'data_class' => null,
                'required' => false
            ))
            ->add('isActive', CheckboxType::class, [
                'required' => false,
                'label' => 'Is active?'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Banner::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_banner';
    }
}
