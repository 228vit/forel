<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Vendor;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vendor', EntityType::class, array(
                'class' => Vendor::class,
                'choice_label' => 'name',
                'required' => false,
                'expanded' => false,
            ))
            ->add('vendorCode')
            ->add('barCode')
            ->add('productGroup')
            ->add('category', EntityType::class, array(
                'class' => 'App:Category',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->addOrderBy('c.root', 'ASC')
                        ->addOrderBy('c.lft', 'ASC');
                },
                'choice_label' => 'forTree',
                'expanded' => false,
            ))

            ->add('name')
            ->add('fullName')
            ->add('price')
            ->add('priceOld')
            ->add('priceOpt')
            ->add('priceIn')
            ->add('slug', TextType::class, [
                'data_class' => null,
                'required' => false,
            ])
            ->add('warranty')
            ->add('metaDescription')

            ->add('description', CKEditorType::class, [
                    'config' => array(
                        'uiColor' => '#ffffff',
                        'filebrowserBrowseRoute' => 'elfinder',
                        'filebrowserBrowseRouteParameters' => array(
                            'instance' => 'default',
                            'homeFolder' => ''
                        )
                    ),
                ]
            )
            ->add('picFile', FileType::class, array(
                'label' => 'Image file',
                'data_class' => null,
                'required' => false
            ))
            ->add('isActive', CheckboxType::class, ['required' => false])
            ->add('inAction', CheckboxType::class, ['required' => false])
            ->add('status')
            ->add('showOnMarket', CheckboxType::class, ['required' => false])
            ->add('showOnTop', CheckboxType::class, ['required' => false])
            ->add('status', ChoiceType::class, [
                'choices'  => Product::getFormStatuses(),
                'expanded' => true,
            ])
//            ->add('productFilterValues', EntityType::class, [
//                'class' => CategoryFilterValue::class,
//                'choice_label' => 'name',
//                'expanded' => true,
//            ])
        ;


        $builder->add('images', CollectionType::class, [
            'label' => false,
            'entry_type' => ProductImageType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'allow_delete' => true,
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Product::class,
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
//    public function getBlockPrefix()
//    {
//        return 'product';
//    }


}
