<?php

namespace App\Form;

use App\Entity\Feedback;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class FeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true
            ])
            ->add('email', EmailType::class, [
                'required' => true
            ])
            ->add('phone', TextType::class, [
                'required' => true
            ])
            ->add('message', TextareaType::class, [
                'required' => true
            ])
        ;
//        $builder->add('recaptcha', EWZRecaptchaType::class, array(
//            'attr' => array(
//                'options' => array(
//                    'theme' => 'light',
//                    'type'  => 'image',
//                    'size' => 'invisible',              // set size to invisible
//                    'defer' => true,
//                    'async' => true,
//                    'callback' => 'onReCaptchaSuccess', // callback will be set by default if not defined (along with JS function that validate the form on success)
//                    'bind' => 'btn_submit',             // this is the id of the form submit button
//                    // ...
//                )
//            ),
//            'mapped'      => false,
//            'constraints' => array(
//                new RecaptchaTrue()
//            )
//        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Feedback::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'feedback';
//        return 'item_filter';
    }
}
