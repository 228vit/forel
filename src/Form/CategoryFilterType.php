<?php

namespace App\Form;

use App\Entity\CategoryFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category')
            ->add('name')
            ->add('position')
        ;
        $builder->add('filterValues', CollectionType::class, [
            'label' => false,
            'entry_type' => CategoryFilterValueType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'allow_delete' => true,
//            'by_reference' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CategoryFilter::class,
        ]);
    }
}
